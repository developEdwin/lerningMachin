lerningMachin
=============

Github para el futuro curso estilo EDX de los clubes de ciencia.

Basado en "La historia entre los datos".
Este club es un curso introductorio a python, webscraping, machine learning y visualizacion de datos. La idea es usar estas herramientas para buscar significado detrás de los datos. Estaremos usando ejemplos tipicos dentro de la literatura y también datos que extraemos el primer dia de páginas de internet en México, la idea es utilizar varias herramientas para ver si podemos encontrar algun historia en ellos. 
Muchas veces nuestros datos son gigantescos, humanamente imposibles de comprender, para ellos utilizaremos Machine Learning, entrenaremos un programa de computadora para sacar la información relevante.

Modulos
=============
* Introduccion, el panorama global.
* Python, solo lo necesario.
* Webscrping, como sacamos datos del internet? (y de manera masiva!)
* Machine Learning: Tecnicas para agrupas y encontrar patrones entre los datos.
  * Regression : Como ajustamos curvas y modelos a datos.
  * Clustering: SVM 
  * Clustering: Kmeans
  * Dimensionality Reduction : PCA
  * Redes Neuronales
* Visualizacion de datos, como presentar los patrones e informacion de manera efectiva.
