# -*- coding:utf-8 -*-
#!/usr/bin/python
import bs4, urllib2, re


def crearSopa(aurl):
	paginaUrl = urllib2.urlopen(aurl)
	paginaHtml = paginaUrl.read()
	paginaUrl.close()
	return bs4.BeautifulSoup(paginaHtml)

def removerEtiquetas(data):
  p = re.compile(r'<.*?>')
  return p.sub('', data)

def uniTexto(texto): 
	return unicode.join(u'\n', map(unicode, texto))
	 
# === Crear sopa para la página de internet. ===

sopa = crearSopa("http://codigoespagueti.com/noticias/ubicacion-via-lactea-universo/")

# === Podemos 'embellecer' un poco la forma de la sopa que hemos creado.

#print sopa.prettify

# === Podemos buscar la fecha en que se hizo el artículo.

fecha = sopa.find_all('span', {'class':'date'})
fecha = str(uniTexto(fecha[0]))
fecha = ''.join(c for c in fecha if c.isdigit())
fecha = [fecha[c:c+2] for c in xrange(0, len(fecha), 2)]
fecha = '.'.join(fecha)


# === Procedemos a buscar el título del documento. === 

titulo = sopa.title
titulo = removerEtiquetas(uniTexto(titulo))


# === Procedemos a buscar entre la sopa aquellas etiquetas que tienen el texto.

texto_articulo = sopa.find_all('p')
texto_articulo = uniTexto(texto_articulo[:-18])
texto_articulo = removerEtiquetas(texto_articulo)

# === Ahora solo juntamos todo en un archivo.

with open('texto_ejemplo-via-lactea.txt', 'wb+') as txt:
	txt.write(titulo.encode('utf-8') + "\n\n")
	txt.write(fecha.encode('utf-8') + "\n\n")
	txt.write(texto_articulo.encode('utf-8'))